# DTU 10MW Reference Wind Turbine

This is the primary project page for the DTU 10MW RWT.

For instructions on how to download the data for the turbine visit The DTU 10MW RWT Wiki.

Should you have any questions regarding the turbine, report or data please raise an issue [here](https://rwt.windenergy.dtu.dk/dtu-10mw-rwt/dtu-10mw-rwt/issues).
